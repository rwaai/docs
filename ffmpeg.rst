ffmpeg
======

Command line based tool for manipulation of media files (convert, concatenate, extract...).

Installation
------------

* Binaries: `<http://ffmpeg.org>`_

* Via command line package managers:

  * Mac Homebrew: ``brew install ffmpeg``
  
  * Win Chocolatey: ``choco install ffmpeg``
