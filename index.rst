RWAAI Guides and Workflows
==========================

This is a continuously updated collection of guides and workflows set up both for internal and external use for the `RWAAI <http://www.lu.se/rwaai>`_ project at Lund University.

.. toctree::
  :maxdepth: 2
  :caption: Tools
  
  imdi_converter_detailed
  ffmpeg